FROM python:3.5
ENV PYTHONUNBUFFERED 1
RUN mkdir /config
ADD /config/requirement.pip /config/
RUN pip install -r /config/requirement.pip
RUN mkdir /src;
WORKDIR /src